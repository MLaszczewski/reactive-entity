defmodule Reactive.LogEntity do

  defmacro __using__(_opts) do
    quote location: :keep do
      use Reactive.Entity

      @log_storage_module Application.get_env(:reactive_entity, :log_storage_module, :storage_not_configured)

      def retrive(id) do
        case Reactive.Entities.retrive_entity(id) do
          :not_found -> :not_found
          {:ok,%{state: state, container: container}} ->
            nstate = Map.put(state, :log, @log_storage_module.open(id))
            {:ok,%{state: nstate, container: container}}
        end
      end

      def init_log(state,id) do
        log = @log_storage_module.init(id)
        state
          |> Map.put(:log,log)
          |> Map.put(:uniq,0)
      end

      def save(id,state,container) do
        sstate=Dict.delete(state,:log)
        Reactive.Entities.save_entity(id,sstate,container)
        :ok
      end

      def get_log_key(timestamp,uniq) do
        :erlang.iolist_to_binary([:io_lib.format("~14.10.0B",[timestamp]),"|",uniq])
        #Integer.to_string(timestamp)<>"|"<>uniq
      end

      def add_to_log(state,timestamp,uniq,data) do
        key=get_log_key(timestamp,uniq)
        @log_storage_module.push(state.log,key,data)
        key
      end

      def remove_from_log(state,timestamp,uniq) do
        @log_storage_module.remove(state.log,get_log_key(timestamp,uniq))
      end

      def overwrite_log(state,timestamp,uniq,data) do
        key=get_log_key(timestamp,uniq)
        @log_storage_module.overwrite(state.log,key,data)
      end

      def update_log(state,timestamp,uniq,update_fun) do
        key=get_log_key(timestamp,uniq)
        {:ok,data}=Reactive.LogsDb.get(state.log,key)
        ndata=update_fun.(data)
        @log_storage_module.overwrite(state.log,key,ndata)
        ndata
      end

      def update_log(state,key,update_fun) do
        {:ok,data}=Reactive.LogsDb.get(state.log,key)
        ndata=update_fun.(data)
        @log_storage_module.overwrite(state.log,key,ndata)
        ndata
      end

      def scan(state,from,to,limit \\ 1_000,reverse \\ false) do
        f=case from do
            :begin -> :begin
            :end -> :end
            i -> Integer.to_string(i)
          end
        t=case to do
            :begin -> :begin
            :end -> :end
            i -> Integer.to_string(i)
          end
        sr=@log_storage_module.fetch(state.log,f,t,limit,reverse)
        Enum.map(sr,fn({sr,v}) ->
          [sts,suq]=String.split(sr,"|")
          {Integer.parse(sts),Integer.parse(suq),v}
        end)
      end

      def raw_scan(state,from,to,limit \\ 1_000,reverse \\ false) do
        @log_storage_module.fetch(state.log,from,to,limit,reverse)
      end
    end
  end
end